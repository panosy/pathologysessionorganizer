#########################################################
# Mayo Clinic Pathology Session Organizer
# Breath Research Inc.
# Panagiotis Giotis
#########################################################

import openpyxl
import glob
import shutil
import os.path
import json
# import numpy as np
import numbers
# import decimal
from os.path import expanduser
# from operator import add



def iter_rows(ws):
    for row in ws.iter_rows():
        yield [cell.value for cell in row]


def readAnalysisJSON(sessionPath, sessionName, sessionFullDataEntry):

    inputJSONpath = sessionPath + 'output.json'

    print (inputJSONpath)

    # Dynamic tag naming from analysis JSON, global for use in exports
    global centralizedSessionDatabaseFEWtags

    if not os.path.exists(inputJSONpath):
        print ('---> No analysis JSON found for session ' + str(sessionName) + ' in ' + str(inputJSONpath))
        return -1

    json_data = open(inputJSONpath)
    data = json.load(json_data)
    print (data)
    JSONkeys = []
    JSONvalues = []
    print (data)
    for key, value in data["SVCdescriptors"].items():
        JSONkeys.append(str(key))
        JSONvalues.append(round(value, 6))
        # print key, value

    # Get condition from JSON
    condition = str(data['sessionInfo']['pathology'])

    # Put proper tags in XLS (temp until all analysis is with the same descriptor version)
    if condition != 'Healthy': 
        centralizedSessionDatabaseFEWtags = JSONkeys

    ##########################
    if updateCentralDB:

        print ('-> Reading session-specific JSON analysis and updating central XLS file...')
        # Append the dynamically-extracted JSON analysis values
        sessionFullDataEntry.append('*')
        sessionFullDataEntry += JSONvalues #REM!

    # Create individual per-session analysis XLS sheet with imagery
    if createSessionAnalysisSheets:
        print ('-> Creating per-session personalized analysis XLS (in-folder)...')
        # Write-out session XLS
        wb = openpyxl.Workbook()
        ws = wb.active
        ws.title = "BRI Analysis for " + sessionName

        # Adjust XLS column width for presentation
        ws.column_dimensions['A'].width = 30
        ws.column_dimensions['B'].width = 20

        # Read-in and insert the spectrograms if present
        filename = sessionPath + 'specMagnified.png'
        if os.path.isfile(filename):
            img = openpyxl.drawing.image.Image(filename)
            ws['A20'] = 'Session Spectrogram'
            img.anchor(ws['A22'])
            ws.add_image(img)

        filename = sessionPath + 'specWheeze.png'
        if os.path.isfile(filename):
            img2 = openpyxl.drawing.image.Image(filename)
            ws['A52'] = 'Wheeze Spectrogram'
            img2.anchor(ws['A54'])
            ws.add_image(img2)

        # XLSX write-out for individual session analysis
        wb.save(sessionPath + '/' + sessionName + 'AnalysisReport.xlsx')

#######################################################################################################################

##################################################
# Pathology Analysis main script                 #
##################################################

###### Control flags ######
# Flag to disable writing for testing and expansion
writeAudioFromMainToDirStructure = False
# Compress audio to m4a/mp3 to save dropBox space
compressAudio = True
# Flag to print debug output
debugOutputEnabled = False
# Flag to create/update centralized xls with breezy data & analysis
updateCentralDB = True
# Flag to create session-wise analysis sheet
createSessionAnalysisSheets = False
# Flag to write only Asthma sessions & Analysis + averages
conditionExtractionModeDB = 'Full'  # Set 'Full', 'COPD', 'Asthma' or 'Healthy'
# Test protocol
testProtocol = 'SVC'

centralizedSessionDatabase = []


print ('########################################################\n'
       '### BRI Pathology Session Organizer v1.0 running...  ###\n'
       '########################################################')

# Create paths for read/write
home = expanduser("~")
sourcePath = basePath = home + '/Dropbox/BreathResearch/Mayo/Subject data/'
# Main demographics xls path containing session inventory, pathology and severity information
demographicsPath = basePath + 'Demographics BreathResearch2.xlsx'
# Base path for initial session distribution according to main xls details (unedited)
baseTargetPath = home + '/Dropbox/BreathResearch/Mayo/BRI/TrainData/Mouthpiece/'
# Base path for edited audio, json with analysis output etc.
# baseEditPath = home + '/Dropbox/BreathResearch/Mayo/BRI/TrainDataEdit/Mouthpiece/'

# Open xlsx file containing session listing and details
wb = openpyxl.load_workbook(demographicsPath)
ws = wb.active

# Initiate vectors
subjectID, subjectNumericID, sex, age, height, weight, condition, severity, processedStatus, lastElement = ([] for i in range(10))

# Search for last valid subject in XLS file #optimize
i = 0

while lastElement is not None:
    i = i+1
    lastElement = ws.cell(row=i, column=2).value

if debugOutputEnabled:
    print ('*** Mayo XLS index contains ' + str(i) + ' session entries')

# Read in values
for idx in range(2, i):
    # print(x, ws.cell(row=x, column=4).value)
    subjectID.append(ws.cell(row=idx, column=1).value)
    subjectNumericID.append(str(idx-1).rjust(4, '0'))  # Convert id to proper 4-digit format
    sex.append(ws.cell(row=idx, column=2).value)
    age.append(ws.cell(row=idx, column=4).value)
    height.append(ws.cell(row=idx, column=5).value)
    weight.append(ws.cell(row=idx, column=6).value)
    condition.append(ws.cell(row=idx, column=8).value)
    severity.append(ws.cell(row=idx, column=9).value)
    processedStatus.append(ws.cell(row=idx, column=12).value)


# Eliminate None types from xlsx ## CHECK FOR REPLACEMENTS!
condition = [0 if v is None else v for v in condition]
severity = [0 if v is None else v for v in severity]

##################################################
# Condition: 1=Asthma; 2=COPD; 3=CF; 4= Healthy  #
# Severity 1 =mild; 2=mod; 3=severe              #
##################################################

# Condition mapping: Convert condition to corresponding str for path creation
x = map(str, (map(int, condition)))
x = [w.replace('1', 'Asthma') for w in x]
x = [w.replace('2', 'COPD') for w in x]
x = [w.replace('3', 'CF') for w in x]
x = [w.replace('4', 'Healthy') for w in x]

# Severity mapping: Convert severity to corresponding str for path creation
y = map(str, (map(int, severity)))
y = [w.replace('0', 'none') for w in y] # Severity 0 is Condition->healthy
y = [w.replace('1', 'mild') for w in y]
y = [w.replace('2', 'moderate') for w in y]
y = [w.replace('3', 'severe') for w in y]

print ('---> Running organizer in ' + str(conditionExtractionModeDB) + ' mode.')

print (x)
########### MAIN LOOP FOR EACH SESSION ############
# Loop for each subject and categorize
for i in range(0, len(subjectID)):

    # Check for specific subset extraction into DB based on pathology
    if conditionExtractionModeDB == 'Asthma' and x[i] != 'Asthma':
        continue
    if conditionExtractionModeDB == 'COPD' and x[i] != 'COPD':
        continue
    if conditionExtractionModeDB == 'Healthy' and x[i] != 'Healthy':
        continue

    print ('*******************************************************')
    print ('-> Analyzing subject # ' + str(i+1))

    # Compensate for 2-digit naming
    if i < 9:
        extra = 0
    else:
        extra = ''

    sourcePath = basePath + 'Subject' + str(extra) + str(i+1)
    # Check if directory exists
    if not os.path.exists(sourcePath):
        print ('*!*!* SourcePath ' + sourcePath + ' does not exist, skipping...')
        continue

    # Add condition & severity in folder structure
    targetPath = baseTargetPath + x[i] + '/' + y[i] + '/'
    # baseEditSessionPath = baseEditPath + x[i] + '/' + y[i] + '/'

    if updateCentralDB:
        print ('-> Importing XLS Mayo data in central DB table...')

        # Create main list entry for this session
        sessionFullDataEntry = [subjectNumericID[i], str(subjectID[i]), str(sex[i]), str(age[i]), str(height[i]),
                                str(weight[i]), x[i], y[i]]

        breezeDataSessionPath = sourcePath + '/Subject' + str(extra) + str(i+1) + '.xlsx'

        # Open XLS file containing session breeze data
        if os.path.isfile(breezeDataSessionPath):
            if debugOutputEnabled:
                print ('-> Importing Mayo breeze data in central DB table for session ' + str(breezeDataSessionPath))

            wb2 = openpyxl.load_workbook(breezeDataSessionPath)
            wsFVC = wb2["FVC"]
            wsSVC = wb2["SVC"]


            spirometrySeverityResult = wsFVC.cell(None, 1, 1).value
            maxFVC = wsFVC.cell(None, 5, 6).value
            percentageFVC = wsFVC.cell(None, 5, 7).value
            absoluteFEV1 = wsFVC.cell(None, 5, 8).value
            percentageFEV1 = wsFVC.cell(None, 5, 9).value

            absoluteSVC = wsSVC.cell(None, 8, 6).value
            percentageSVC = wsSVC.cell(None, 8, 7).value
            absoluteIC = wsSVC.cell(None, 8, 8).value
            percentageIC = wsSVC.cell(None, 8, 9).value
            absoluteERV = wsSVC.cell(None, 8, 10).value
            percentageERV = wsSVC.cell(None, 8, 11).value

            spirometrySeverityResult = 0

            sessionFullDataEntry.extend(('*', absoluteSVC, percentageSVC, absoluteIC, percentageIC, absoluteERV, percentageERV,
                                         spirometrySeverityResult, maxFVC, percentageFVC, absoluteFEV1, percentageFEV1))


        else:
            sessionFullDataEntry.extend(('*', '', '', '', '', '', '', '', '', '', '', ''))


    # SVC
    # files = glob.iglob(os.path.join(sourcePath, "Mouthpiece*' + testProtocol +'*.m4a"))
    txtLine = 'Mouthpiece*' + testProtocol + '*.m4a'
    files = glob.iglob(os.path.join(sourcePath, txtLine))


    for file in files:

        # Get just the session filename (no extension, no whitespace)
        mayoSubjectID = os.path.splitext(os.path.basename(file))[0]
        # Remove all whitespace from ID (DOUBLE CHECK!!!)
        mayoSubjectID = mayoSubjectID.replace(" ", "")
        # finalSubjectID = 'subject' + subjectNumericID[i] + '_' + mayoSubjectID
        finalSubjectID = 'subject' + subjectNumericID[i]

        if os.path.isfile(file):
            targetSessionPath = targetPath + testProtocol + '/' + finalSubjectID + '/'
            # baseEditSessionPath = baseTargetPath + 'FVC/' + finalSubjectID + '/'

            # Print debug
            if debugOutputEnabled:
                print ('** Original Audio Path: ' + file)
                print('** Session ID (Mayo, stripped): ' + mayoSubjectID)
                print('** Session ID (Numeric): ' + finalSubjectID)
                print ('** Target session path: ' + targetSessionPath)
                # print ('** BaseEditSessionPath: ' + baseEditSessionPath)

            if not os.path.exists(targetSessionPath):
                print('-> Creating analysis folder for ' + finalSubjectID + ' at ' + targetSessionPath)
                os.makedirs(targetSessionPath)
            else:
                print ('-> Analysis folder for session ' + finalSubjectID + ' is already present...')

            if writeAudioFromMainToDirStructure:
                print('-> Writing files from central (MAYO) to BRI dir tree...')
                shutil.copy2(file, targetSessionPath)

            if createSessionAnalysisSheets or updateCentralDB:
                print('-> Creating session-specific analysis sheet (in-folder) for ' + finalSubjectID)
                readAnalysisJSON(targetSessionPath, finalSubjectID, sessionFullDataEntry)


    # Check if full session analysis is present and append (omit incomplete entries)
    if len(sessionFullDataEntry) > 16:
        centralizedSessionDatabase.append(sessionFullDataEntry)


if updateCentralDB:

    print ('-> Writing centralized DB table and XLS...')
    centralizedDBpath = basePath
    wbExport = openpyxl.Workbook()
    ws = wbExport.active

    # Create title row for XLS output

    # Static part (session info + Breeze data)
    sessionFullDataEntryTitles = ['Subject numeric ID', 'Mayo SubjectID', 'Sex (M=1)', 'Age', 'Height (cm)', 'Weight (Kg)',
                                  'Condition', 'Severity', 'BREEZE DATA',
                                  'absoluteSVC', 'percentageSVC', 'absoluteIC', 'percentageIC', 'absoluteERV', 'percentageERV',
                                  'Spirometry severity', 'maxFVC', 'percentageFVC', 'absoluteFEV1', 'percentageFEV1',
                                  'FEW DATA']

    # Add the dynamically-extracted FEW tags for XLS writing
    sessionFullDataEntryTitles.extend(centralizedSessionDatabaseFEWtags)
    ws.append(sessionFullDataEntryTitles)

    ####################################################################################################################
    ### AVERAGING MODULE
    ncols = len(centralizedSessionDatabase[0])
    nrows = len(centralizedSessionDatabase)
    print ('columns : ' + str(ncols))
    print ('rows : ' + str(nrows))

    averageValues = ncols * [0]  # avgs per column
    numberOfValidSessions = float(nrows)
    divider = numberOfValidSessions

    # Define non-numeric/irrelevant columns for averaging
    nonValidList = [0, 1, 2, 3, 4, 5, 6, 7, 8, 20, 21]


    # Iterate columns and extract average values (omit non-numeric columns)
    for col in xrange(ncols):
        if col not in nonValidList:
            # print ('Column ' + str(col) + ' is valid')
            # print ('row length is: ' + str(len(centralizedSessionDatabase[col][0])))
            for row in xrange(nrows):
                # print (centralizedSessionDatabase[row][col])
                # centralizedSessionDatabase[row][col].replace('.', '', 1).isdigit()
                # if col == 31:
                #     print ('val is: ' + str(centralizedSessionDatabase[row][col]))
                # >> > [isinstance(x, numbers.Number) for x in (0, 0.0, 0j, decimal.Decimal(0))]

                # print (centralizedSessionDatabase[row][col])

                if isinstance(centralizedSessionDatabase[row][col], numbers.Number): #for x in (0, 0.0, 0j, decimal.Decimal(0))
                # if type(centralizedSessionDatabase[row][col]) == str or centralizedSessionDatabase[row][col] == '':
                    # print (divider)
                    averageValues[col] += centralizedSessionDatabase[row][col]
                else:
                    divider = divider - 1
                    continue

            if divider != 0:
                averageValues[col] = round (averageValues[col] / divider, 5)
            divider = numberOfValidSessions
        else:
            averageValues[col] = ''

    print ('numberOfValidSessions: ' + str(divider))
    averageValues[0] = 'Average Values'
    print averageValues
    ####################################################################################################################

    # Append the database and average values to the XLSX WorkSheet
    for x in range(0, len(centralizedSessionDatabase)):
        ws.append(centralizedSessionDatabase[x])
    ws.append(averageValues)

    # Create filename and WS titles according to selected mode
    centralizedDBpath = centralizedDBpath + 'centralizedDB' + conditionExtractionModeDB + testProtocol + '.xlsx'
    ws.title = testProtocol + ' BRI Pathology ' + conditionExtractionModeDB + ' DB'
    print ('-> Storing DB XLSX at: ' + str(centralizedDBpath))

    # Auto-adjust column width for XLS presentation
    for col in ws.columns:
        max_length = 0
        column = col[0].column  # Get the column name
        for cell in col:
            try:  # Necessary to avoid error on empty cells
                if len(str(cell.value)) > max_length:
                    max_length = len(cell.value)
            except:
                pass
        adjusted_width = (max_length + 2) * 1
        ws.column_dimensions[column].width = adjusted_width

    # Save DB to file
    wbExport.save(centralizedDBpath)

print ('*** Organizer successfully completed! Parsed and processed ' + str(len(centralizedSessionDatabase)) +
       ' valid sessions...')
